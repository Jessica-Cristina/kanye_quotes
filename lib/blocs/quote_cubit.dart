import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kenye_app/data/models/kenye_model.dart';
import 'package:kenye_app/data/repositories/quote_repository_interface.dart';

class QuoteState {
  KenyeModel get quote => KenyeModel('');
}

class InicialState extends QuoteState {
  @override
  KenyeModel get quote => KenyeModel('');
}

class LoadingState extends QuoteState {
  @override
  KenyeModel get quote => KenyeModel('');
}

class ErrorState extends QuoteState {
  @override
  KenyeModel get quote => KenyeModel('');
}

class SuccessState extends QuoteState {
  final KenyeModel quot;

  SuccessState(this.quot);

  @override
  KenyeModel get quote => quot;
}

class QuoteCubit extends Cubit<QuoteState> {
  final QuoteRepositoryInterface _repositoryInterface;

  QuoteCubit(this._repositoryInterface) : super(InicialState()) {
    getQuote();
  }

  Future<KenyeModel?> getQuote() async {
    try {
      emit(LoadingState());
      final quot = await _repositoryInterface.getKenyeQuote();
      emit(SuccessState(quot));
    } on Exception {
      emit(ErrorState());
    }
  }
}
